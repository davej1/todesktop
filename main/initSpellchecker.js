const appOptions = require("../appConfig.json");
const { session } = require("electron");
const { log } = require("../common/logger");

// ISO 639-1 standard language codes
const LANGUAGES = ['en-US', 'ru', 'fr', 'pt', 'it', 'sv', 'de', 'es'];

const ELECTRON_VERSION = process.versions.electron;

module.exports = () => {
  if (!appOptions.spellchecker) {
    return;
  }

  if (!session.defaultSession.setSpellCheckerLanguages) {
    log("main", `Spellcheck not supported on version ${ELECTRON_VERSION}`);
    return;
  }

  const supportedLanguages = session.defaultSession.availableSpellCheckerLanguages || [];
  const languages = LANGUAGES.filter(lang => supportedLanguages.includes(lang));

  session.defaultSession.setSpellCheckerLanguages(languages);
};
