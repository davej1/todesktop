const electron = require("electron");

const app = electron.app || electron.remote.app;

module.exports = Object.freeze({
  // We can't use electron-is-dev package because we disable remote in the renderer
  isDev: !app.isPackaged,
});
